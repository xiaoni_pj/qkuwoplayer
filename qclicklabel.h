﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qclicklabel.h --- QClickLabel
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/19
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/19
 *******************************************************************/
#ifndef QCLICKLABEL_H
#define QCLICKLABEL_H

#include <QLabel>

class QClickLabel : public QLabel
{
    Q_OBJECT
public:
    explicit QClickLabel(QWidget *parent = nullptr);
    ~QClickLabel();

signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent *ev) override;
};

#endif // QCLICKLABEL_H
