﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : %{Cpp:License:FileName} --- %{Cpp:License:ClassName}
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/15
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/15
 *******************************************************************/
#include "mainwindow.h"
#include "qhttpdownload.h"

#include <QApplication>
#include <QFontDatabase>
#include <QMessageBox>
#include <QScreen>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QFontDatabase::addApplicationFont(":/res/font/iconfont.ttf");
    qApp->setStyleSheet("file:///:/res/qss/white.qss");
#if 0
    // check version
    QString strInfo;
    bool bOk = QHttpDownload::checkAppVersion(APP_NAME, APP_VERSION, strInfo);
    if (bOk)
    {
        if (QMessageBox::No == QMessageBox::question(NULL, "程序更新", QString("检查到更新\n【%1】，\n是否立即下载?").arg(strInfo)))
        {
            return 0;
        }
    }
#endif
    MainWindow w;
#if 1
    QSize _desktopSize = qApp->desktop()->geometry().size();
    int x = (_desktopSize.width() - w.width()) * 0.5;
    int y = (_desktopSize.height() - w.height()) * 0.5;
    w.move(x, y);
#endif


    w.show();
    return a.exec();
}
