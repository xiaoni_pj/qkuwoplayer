﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qwidgetbase.h --- QWidgetBase
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/9
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/9
 *******************************************************************/
#ifndef QWIDGETBASE_H
#define QWIDGETBASE_H

#include <QWidget>
#include <QLabel>
#include <QBoxLayout>

class QWidgetBase : public QWidget
{
    Q_OBJECT
public:
    explicit QWidgetBase(QWidget *parent = nullptr);
    ~QWidgetBase();

protected:
#ifndef _MSC_VER
    bool nativeEvent(const QByteArray &eventType, void *message, long *result) override;
#endif
    void changeEvent(QEvent* e) override;
    void paintEvent(QPaintEvent *) override;

private:
    float m_dpiScale = 1.0f;

private:
    void setWidgetBorderless();
};

#endif // QWIDGETBASE_H
