﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qmusiclist.cpp --- QMusicList
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/15
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/15
 *******************************************************************/
#include "qmusiclist.h"

#include <QMouseEvent>
#include <QPainter>
#include <QDebug>

#define GB      (1024 * 1024 * 1024)
#define MB      (1024 * 1024)
#define KB      (1024)

QMusicList::QMusicList(QWidget *parent) : QAbstractListWidget(parent)
{
    m_isWait = true;
    m_mode = 0;
    m_backgroundColor = "#fafafa";
    m_textColor       = "#333333";
    m_itemSize = 50;
    m_download = QRect(0, 0, 0, 0);
    m_playBtn  = QRect(0, 0, 0, 0);
}

QMusicList::~QMusicList()
{

}

void QMusicList::setWait(bool bOk)
{
    if (bOk != m_isWait)
    {
        m_isWait = bOk;
        viewport()->update();
    }
}

void QMusicList::setListMode(int _mode)
{
    m_mode = _mode;
}

void QMusicList::paintEvent(QPaintEvent *)
{
    QPainter painter(viewport());
    painter.setRenderHints(QPainter::Antialiasing);
    painter.fillRect(viewport()->rect(), m_backgroundColor);
    painter.setPen(m_textColor);
    if (m_isWait)
    {
        drawWaitinfo(&painter);
    }
    else if (!m_items.isEmpty())
    {
        foreach(QAbstractListItem *_item, m_items) {
            if (_item->rect().width() > 0 && _item->rect().bottom() > 0) {
                drawItemInfo(&painter, _item);
            }
        }
    }
}

void QMusicList::drawItemInfo(QPainter *painter, QAbstractListItem *_item)
{
    painter->save();
    QMusicItem *item = qobject_cast<QMusicItem *>(_item);
    QRect rect = _item->rect();
    painter->setPen("#333333");
    if (_item->isHover() || _item->isChecked())
    {
        painter->fillRect(rect, QColor("#11000000"));
    }

    QRect rectIcon(rect.left(), rect.top(), 40, rect.height());
    drawIconfont(painter, rectIcon, QChar(0xe612), "#ff6a6a");
    int nW = rect.width() * 0.5 - rectIcon.width();
    QRect rectTxt(rectIcon.right(), rect.top(), nW, rect.height());
    painter->setPen(m_textColor);
    painter->drawText(rectTxt, Qt::AlignVCenter, item->song());

    if (_item->isHover())
    {
        rectIcon = QRect(rectTxt.right() - 40 - 20, rectTxt.top(), 40, rectTxt.height());
        drawIconfont(painter, rectIcon, QChar(0xe679), "#8a8a8a", 24);

        if (0 == m_mode)
        {
            rectIcon = QRect(rectIcon.left() - 30, rectIcon.top(), 30, rectIcon.height());
            m_download = rectIcon;
            drawIconfont(painter, rectIcon, QChar(0xe635), "#8a8a8a", 24);
        }

        rectIcon = QRect(rectIcon.left() - 30, rectIcon.top(), 30, rectIcon.height());
        m_playBtn = rectIcon;
        drawIconfont(painter, rectIcon, QChar(0xe717), "#8a8a8a", 30);
    }

    nW = rect.width() * 0.16;
    rectTxt = QRect(rectTxt.right(), rectTxt.top(), nW, rectTxt.height());
    painter->drawText(rectTxt, Qt::AlignVCenter, item->singer());

    rectTxt = QRect(rectTxt.right(), rectTxt.top(), nW, rectTxt.height());
    painter->drawText(rectTxt, Qt::AlignVCenter, item->album());

    rectTxt = QRect(rectTxt.right(), rectTxt.top(), rect.width() - rectTxt.right(), rectTxt.height());
    painter->setPen(QColor("#ababab"));
    if (1 == m_mode)
    {
        painter->drawText(rectTxt, Qt::AlignCenter, item->sizeUnit());
    }
    else {
        painter->drawText(rectTxt, Qt::AlignCenter, item->durationTime());
    }

    painter->restore();
}

void QMusicList::drawWaitinfo(QPainter *painter)
{
    painter->save();
    painter->drawText(viewport()->rect(), Qt::AlignCenter, QStringLiteral("正在加载,请稍后..."));
    painter->restore();
}

void QMusicList::clearRects()
{
    m_download = QRect(0, 0, 0, 0);
}

void QMusicList::mousePressEvent(QMouseEvent *e)
{
    if (m_download.contains(e->pos()))
    {
        QMusicItem *_item = qobject_cast<QMusicItem *>(m_items.value(m_hoverIndex));
        emit download(_item->albumId(), _item->hash());
    }

    if (m_playBtn.contains(e->pos()))
    {
        if (m_items.contains(m_hoverIndex))
        {
            QMusicItem *_item = qobject_cast<QMusicItem *>(m_items.value(m_hoverIndex));
            emit musicChanged(_item);
        }
    }

    QAbstractListWidget::mousePressEvent(e);
}

/////////////////////////////////////////////////////////////
QMusicItem::QMusicItem(int _id) : QAbstractListItem()
{
    m_id = _id;
    m_hover = false;
    m_duration = 0;
    m_size = 0;
}

QString QMusicItem::song() const
{
    return m_song;
}

void QMusicItem::setSong(const QString &song)
{
    m_song = song;
}

QString QMusicItem::hash() const
{
    return m_hash;
}

void QMusicItem::setHash(const QString &hash)
{
    m_hash = hash;
}

QString QMusicItem::album() const
{
    return m_album;
}

void QMusicItem::setAlbum(const QString &album)
{
    m_album = album;
}

QString QMusicItem::singer() const
{
    return m_singer;
}

void QMusicItem::setSinger(const QString &singer)
{
    m_singer = singer;
}

QString QMusicItem::durationTime()
{
    return QString("%1:%2").arg(m_duration / 60, 2, 10, QChar('0')).arg(m_duration % 60, 2, 10, QChar('0'));
}

qint64 QMusicItem::duration() const
{
    return m_duration;
}

void QMusicItem::setDuration(qint64 value)
{
    m_duration = value;
}

QString QMusicItem::imgUrl() const
{
    return m_imgUrl;
}

void QMusicItem::setImgUrl(const QString &imgUrl)
{
    m_imgUrl = imgUrl;
}

QString QMusicItem::playUrl() const
{
    return m_playUrl;
}

void QMusicItem::setPlayUrl(const QString &playUrl)
{
    m_playUrl = playUrl;
}

QString QMusicItem::albumId() const
{
    return m_albumId;
}

void QMusicItem::setAlbumId(const QString &albumId)
{
    m_albumId = albumId;
}

QString QMusicItem::sizeUnit()
{
    QString strText;
    if (m_size > MB)
    {
        strText = QString("%1 M").arg((m_size * 1.0) / MB, 0, 'f', 2);
    }
    else if (m_size > KB)
    {
        strText = QString("%1 KB").arg((m_size * 1.0) / KB, 0, 'f', 2);
    }
    else {
        strText = QString("%1 B").arg(m_size);
    }

    return strText;
}

qint64 QMusicItem::size() const
{
    return m_size;
}

void QMusicItem::setSize(const qint64 &size)
{
    m_size = size;
}
