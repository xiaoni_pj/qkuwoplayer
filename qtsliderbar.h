﻿/******************************************************************
 Copyright (C) 2017 - All Rights Reserved by
 文 件 名 : qtsliderbar.h --- QtSliderBar
 作 者    : Niyh  (QQ:393320854)
 编写日期 : 2019
 说 明    :
 历史纪录 :
 <作者>    <日期>        <版本>        <内容>
           2019/8/30
*******************************************************************/
#ifndef QTSLIDERBAR_H
#define QTSLIDERBAR_H

#include <QWidget>

class QtSliderBar : public QWidget {
    Q_OBJECT
public:
    explicit QtSliderBar(QWidget *parent = 0);
    ~QtSliderBar();

    typedef enum {Horizontal,Vertical} Direction;

    void setHorizontal(bool bOk);
    void setSliderSize(int sliderSize, int handleSize);

    int maximum();
    void setMaxmum(int value);
    void setValue(int value);
    int value();

    void setReadOnly(bool bOk);

    void showHandleBackground(bool bOk);
    void setSlidetColor(const QColor &color);
    void setHandleColor(const QColor &color);
    void setHandleBgColor(const QColor &color);
signals:
    void valueChanged(int value);

private:
    int     m_nDirection;
    int     m_nValue;
    int     m_nMaxmum;

    int     m_nSliderSize;
    int     m_nHandleSize;

    bool    m_bPressed;
    QPoint  m_starPos;
    int     m_nOffset;

    bool    m_bReadOnly;

    QColor  m_colorSlider;
    QColor  m_colorHandle;
    QColor  m_colorHandleBg;
    bool    m_bShowHandleBg;
    bool    m_isHover;
protected:
    void paintEvent(QPaintEvent *) override;
    void drawHorizontalBar(QPainter *painter);
    void drawVerticalBar(QPainter *painter) ;

    void resizeEvent(QResizeEvent *e) override;
    void mousePressEvent(QMouseEvent *e) override;
    void mouseReleaseEvent(QMouseEvent *e) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    void wheelEvent(QWheelEvent *e) override;

    void enterEvent(QEvent *e) override;
    void leaveEvent(QEvent *e) override;
};
#endif // QTSLIDERBAR_H
