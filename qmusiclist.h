﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qmusiclist.h --- QMusicList
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/15
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/15
 *******************************************************************/
#ifndef QMUSICLIST_H
#define QMUSICLIST_H

#include "qabstractlistwidget.h"

class QMusicItem;
class QMusicList : public QAbstractListWidget
{
    Q_OBJECT
public:
    explicit QMusicList(QWidget *parent = nullptr);
    ~QMusicList();

    void setWait(bool bOk);
    void setListMode(int _mode);

signals:
    void audition(const QString &_albumid, const QString &_hash);
    void download(const QString &_albumid, const QString &_hash);
    void musicChanged(QMusicItem *_item);

private:
    bool m_isWait;
    int  m_mode;
    QRect m_download;
    QRect m_playBtn;

protected:
    void paintEvent(QPaintEvent *) override;
    void drawItemInfo(QPainter *painter, QAbstractListItem *_item) override;
    void drawWaitinfo(QPainter *painter);
    void clearRects() override;

    void mousePressEvent(QMouseEvent *) override;
};

//////////////////////////////////////////////////////////
class QMusicItem : public QAbstractListItem
{
    Q_OBJECT
public:
    QMusicItem(int _id);

    QString song() const;
    void setSong(const QString &song);

    QString hash() const;
    void setHash(const QString &hash);

    QString album() const;
    void setAlbum(const QString &album);

    QString singer() const;
    void setSinger(const QString &singer);

    QString durationTime();
    qint64 duration() const;
    void setDuration(qint64 value);

    QString imgUrl() const;
    void setImgUrl(const QString &imgUrl);

    QString playUrl() const;
    void setPlayUrl(const QString &playUrl);

    QString albumId() const;
    void setAlbumId(const QString &albumId);

    QString sizeUnit();
    qint64 size() const;
    void setSize(const qint64 &size);

private:
    QString m_song;
    QString m_hash;
    QString m_album;
    QString m_albumId;
    QString m_singer;
    qint64  m_duration;
    qint64  m_size;

    QString m_imgUrl;
    QString m_playUrl;
};

#endif // QMUSICLIST_H
