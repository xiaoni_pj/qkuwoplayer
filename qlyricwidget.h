﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qlyricwidget.h --- QLyricWidget
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/15
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/15
 *******************************************************************/
#ifndef QLYRICWIDGET_H
#define QLYRICWIDGET_H

#include "qabstractlistwidget.h"
#include <QList>
#include <QPropertyAnimation>
#include <QDialog>
#include <QLinearGradient>

class QDesktopLyric;
class QLyricLine;
class QLyricWidget : public QAbstractListWidget
{
    Q_OBJECT
    Q_PROPERTY(int offset READ offset WRITE setOffset)
public:
    explicit QLyricWidget(QWidget *parent = nullptr);
    ~QLyricWidget();

    bool setLyric(const QString &_fileName);
    void setPosition(qint64 _msec);
    void setMediaState(bool isPause);

    int offset() const;
    void setOffset(int offset);

    void closeDesktop();
    void showDesktopLyric(bool bOk);

    void setAlbumPixmap(const QImage &_img);

signals:

private:
    qint64 m_nTimeoffset;
    int    m_nPosOffset;
    QPropertyAnimation *m_animation;
    QDesktopLyric *m_desktopLyric;

    QImage m_imageBackground;
    QImage m_imageAlbum;
    bool   m_isAmbiguous;
protected:
    void paintEvent(QPaintEvent *) override;
    virtual void drawItemInfo(QPainter *painter, QAbstractListItem *_item) override;
    void resizeEvent(QResizeEvent *e) override;
    void mousePressEvent(QMouseEvent *e) override;
    void mouseDoubleClickEvent(QMouseEvent *e) override;
};

/////////////////////////////////////////////////////////////
class QLyricLine : public QAbstractListItem {
    Q_OBJECT
public:
    QLyricLine(int _id, qint64 _msec, qint64 _stay, const QString &_text);

    qint64 millisecond();
    qint64 staytime();
    QString text() const;

private:
    qint64 m_timeoffset;
    qint64 m_millisecond;
    qint64 m_staytime;
    QString m_lineText;
};

///////////////////////////////////////////////////
class QDesktopLyric : public QDialog {
    Q_OBJECT

    Q_PROPERTY(int mask READ maskWidth WRITE setMaskWidth)

public:
    QDesktopLyric(QWidget *parent = 0);
    ~QDesktopLyric();

    void setLyricText(const QString &_text, qint64 _staytime);

    int maskWidth() const;
    void setMaskWidth(int maskWidth);
    void setStatus(bool isPause);

private:
    QString m_strText;
    int     m_maskWidth;

    QFont m_textFont;
    QLinearGradient m_linearGradient;
    QLinearGradient m_maskGradient;

    QPropertyAnimation *m_animation;
    QRect m_btnClose;

protected:
    void paintEvent(QPaintEvent *) override;
    void drawIconfont(QPainter *painter, const QRect &_rect, QChar _ico, QColor _color, int _size = 20);

    void resizeEvent(QResizeEvent *) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    void mousePressEvent(QMouseEvent *e) override;
    void mouseReleaseEvent(QMouseEvent *) override;
    void enterEvent(QEvent *e) override;
    void leaveEvent(QEvent *e) override;

    bool m_isPressed = false;
    QPoint m_startPos;
    bool m_isHover = false;
};

//////////////////////////////////////////////////////////
class QDesktopLyricPrev : public QWidget {
    Q_OBJECT
public:
    QDesktopLyricPrev(QWidget *parent = 0);

    void setText(const QString &_text);
    void setTextFont(QFont font);
    void setLyricColor(const QColor &_text, const QColor &_mask);

private:
    QString m_strText;
    int     m_maskWidth;

    QFont m_textFont;
    QColor m_linearGradient;
    QColor m_maskGradient;

protected:
    void adjuestSize();
    void paintEvent(QPaintEvent *) override;
};

#endif // QLYRICWIDGET_H
