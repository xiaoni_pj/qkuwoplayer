﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qhttpdownload.h --- QHttpDownload
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/15
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/15
 *******************************************************************/
#ifndef QHTTPDOWNLOAD_H
#define QHTTPDOWNLOAD_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QFile>
#include <QList>

class DownloadItem;
class QHttpDownload : public QObject
{
    Q_OBJECT
public:
    explicit QHttpDownload(QObject *parent = nullptr);
    ~QHttpDownload();

    void download(const QString &_url, const QString &_fileName, const QString &_path);
    static bool checkAppVersion(const QString &_appName, const quint32 &_appVer, QString &_info);

signals:
    void finished(const QString &_fileName);

private slots:
    void sltReadyRead();
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void sltFinished();
    void startTask();

private:
    QNetworkAccessManager   *m_http;
    QNetworkReply           *m_reply;
    QFile                    m_file;
    QList<DownloadItem>      m_downTask;
    bool                     m_isDownload;
};

class DownloadItem {
public:
    DownloadItem() {}
    DownloadItem(const QString &_url, const QString &_name, const QString &_path);

    QString url() const;
    void setUrl(const QString &url);

    QString name() const;
    void setName(const QString &name);

    QString path() const;
    void setPath(const QString &path);

    QString filePath() const;
private:
    int     m_id;
    QString m_url;
    QString m_name;
    QString m_path;
};

#endif // QHTTPDOWNLOAD_H
