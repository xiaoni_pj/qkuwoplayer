#include "qiconfontbutton.h"

#include <QFont>
#include <QDebug>

QIconFontButton::QIconFontButton(QWidget *parent) : QPushButton(parent)
{
    m_iconfont = "";
}

QString QIconFontButton::iconfont() const
{
    return m_iconfont;
}

void QIconFontButton::setIconfont(const QString &_txt)
{
    m_iconfont = _txt;
    this->setText(QChar(_txt.toUInt(0, 16)));
}
