﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qclicklabel.cpp --- QClickLabel
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/19
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/19
 *******************************************************************/
#include "qclicklabel.h"

QClickLabel::QClickLabel(QWidget *parent) : QLabel(parent)
{
    this->setCursor(Qt::PointingHandCursor);
}

QClickLabel::~QClickLabel()
{
}

void QClickLabel::mousePressEvent(QMouseEvent *ev)
{
    emit clicked();
    QLabel::mousePressEvent(ev);
}
