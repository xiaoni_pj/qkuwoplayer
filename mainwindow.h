﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : %{Cpp:License:FileName} --- %{Cpp:License:ClassName}
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/15
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/15
 *******************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMediaPlayer>
#include <QSystemTrayIcon>

#include "qwidgetbase.h"
#include "qhttpdownload.h"
#include "qlyricwidget.h"
#include "qmusiclist.h"
#include "qtnotification.h"

#define APP_VERSION 0x01000001
#define APP_NAME    "Kuwoplayer"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QWidgetBase
{
    Q_OBJECT
    typedef enum { PageSearch, PageLocal, PageLyric, PageConfig, PageCnt} MainPage;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    typedef enum {None, Search, GetSong, Download} Type;

private slots:
    void sltNetworkFinish(QNetworkReply *_reply);

    /* media slots */
    void durationChanged(qint64 duration);
    void positionChanged(qint64 position);
    void stateChanged(QMediaPlayer::State _state);
    void itemClicked(QAbstractListItem *_item);
    void musicChanged(QMusicItem *_item);

    void on_btnSearch_clicked();

    /* UI buttons */
    void on_btnLyric_clicked(bool checked);
    void on_btnPrev_clicked();
    void on_btnNext_clicked();
    void on_btnPlay_clicked();
    void on_btnBack_clicked();
    void on_btnLoadMusic_clicked();
    void sltShowLyricPage();
    void sltProcessChanged(int value);

    void sltLeftButtonClicked(int index);
    void sltChangePage(int index);

    /* music list */
    void auditionSong(const QString &_albumId, const QString &_hash);
    void download(const QString &_albumId, const QString &_hash);
    void downloadFinished(const QString &_fileName);

    void showNotify(const QString &_msg, int _type);
    void on_lineEditSearch_returnPressed();

    void sltSysMenuTrigger(QAction *_action);
    void sltPlayModeChanged(QAction *_action);

    /**/
    void onActivated(QSystemTrayIcon::ActivationReason reason);

private:
    void initMenus();
    void initTrayIcon();
    void checkDirs();
    void loadLocalMusics();
    void showSingerPixmap(const QString &_fileName);
    void showAlbumPixmap(const QString &_fileName);

    QJsonObject getJsonObject(const QByteArray &_byData);

    void searchSong(const QString &_name);
    void parseSearch(const QByteArray &_byData);

    void parseGetsong(const QByteArray &_byData);
    void parseDownload(const QByteArray &_byData);

    void writeLyrics(const QString &_song, const QString &_lyrics);
    void playMusic(const QString &_url, const QString &_name);
    void playCurrentList();

private:
    Ui::MainWindow *ui;
    int m_nCurrentPage;
    int m_nType = None;
    QNetworkAccessManager *m_http;
    QNetworkReply         *m_reply;
    QHttpDownload         *m_download;

    /* music */
    QMediaPlayer          *m_player;
    QMusicItem            *m_currentMusic;

    /* QSystemTrayIcon menu*/
    QSystemTrayIcon       *m_sysTrayicon;

    QtNotification        *m_notify;

protected:
    void resizeEvent(QResizeEvent *e) override;
    bool eventFilter(QObject *obj, QEvent *event) override;
};
#endif // MAINWINDOW_H
