﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qtmenuwidget.h --- QtMenuWidget
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/21
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/21
 *******************************************************************/
#ifndef QTMENUWIDGET_H
#define QTMENUWIDGET_H

#include <QMenu>
#include <QWidget>

class QtMenuWidget : public QMenu
{
    Q_OBJECT
public:
    explicit QtMenuWidget(QWidget *_centerWidget, QWidget *parent = nullptr);
    ~QtMenuWidget();

    typedef enum {Left, Top, Right, Bottom} Direction;
    void setDireciton(int _dir);

signals:

private:
    int m_direction;
    QWidget *m_centerWidget;

protected:
    void paintEvent(QPaintEvent *) override;
};

#endif // QTMENUWIDGET_H
