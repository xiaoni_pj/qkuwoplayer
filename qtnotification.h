#ifndef QTNOTIFICATION_H
#define QTNOTIFICATION_H

#include <QWidget>
#include <QTimer>
#include <QPropertyAnimation>
#include <QMap>

class QtNotifyItem;
class QtNotification : public QObject
{
    Q_OBJECT
public:
    explicit QtNotification(QWidget *parent = nullptr);
    ~QtNotification();
    void setNotification(const QString &msg, int type = 0, int yoffset = 0);
    void setParentRect(const QRect &_rect);
signals:

private slots:
    void slotItemlose();

private:
    QRect m_parentRect;
    QMap<int, QtNotifyItem *> m_msgTips;
    int m_nMaxShowCnt = 5;
    int m_nMsgIndex = 0;
};

///////////////////////////////////////////////////
class QtNotifyItem : public QWidget {
    Q_OBJECT
public:
    explicit QtNotifyItem(int id, const QString &_msg, const int &_type);
    ~QtNotifyItem();

    typedef enum
    {
        MsgInfo,
        MsgOk,
        MsgWarin,
        MsgError,
        MsgCnt,
    } E_MsgType;

    void showMessage(const QPoint &pos);
    void updatePos(const QPoint &pos);

    void setMessage(const QString &_msg);
    int id() const;

protected:
    void paintEvent(QPaintEvent *) override;
    void mousePressEvent(QMouseEvent *e) override;
    void mouseMoveEvent(QMouseEvent *e) override;

    QSize sizeHint() const override;
    void resizeEvent(QResizeEvent *e) override;

signals:
    void closed();

private slots:
    void onValueChanged(const QVariant &value);

private:
    int     m_id;
    QString m_strMsg;
    int     m_msgType;
    QRect   m_btnClose;

    QPixmap m_pixmap;
    QPixmap m_pixmapClose;

    QTimer  m_timer;
    QPropertyAnimation *m_animation;
};

#endif // QTNOTIFICATION_H
