﻿# qkuwoplayer

#### 介绍
现在很多听歌的软件都需要vip，很多还不能下载，作为程序员的我们只能靠自己解决

#### 软件架构
<p>1、目前基于Qt5.14.2-mingw-64开发的，其他版本未测试。</p>
<p>2、本程序网络接口为kugou提供的免费接口。</p>
<p>3、本程序涉及的图标为<a href='https://www.iconfont.cn'>iconfont</p>


#### 功能说明
<p>1、程序启动默认加载本地文件[musics]。后续慢慢完善</p>
<p>2、本程序可以在线直接听歌，也可下载。</p>
<p>3、歌词也是在线直接同步的。</p>


#### 效果展示
![本地歌曲](screeshots/local_music.png)
![网络搜索](screeshots/network_music.png)
![歌词同步](screeshots/lyric.png)
![桌面歌词](screeshots/desktop_lyric.png)
![桌面歌词](screeshots/desktop_lyric_1.png)

#### 版本更新记录
【v1.0.0.2】 2022-09-20
1、更新进度条拉到同步音乐进度，更新进度条UI。
2、更新音乐声音控制控价。
3、添加系统设置界面。
4、添加版本更新检查功能。

【v1.0.0.2】 202209191558
1、更新桌面歌词。由于当前歌词没有停留时间，因此计算的时间可能不准。
2、优化界面相关UI调度。

【v1.0.0.1】
1、初始发布。
