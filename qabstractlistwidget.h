﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qabstractlistwidget.h --- QAbstractListWidget
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/15
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/15
 *******************************************************************/
#ifndef QABSTRACTLISTWIDGET_H
#define QABSTRACTLISTWIDGET_H

#include <QAbstractScrollArea>
#include <QMap>

class QAbstractListItem;
class QAbstractListWidget : public QAbstractScrollArea
{
    Q_OBJECT
    Q_PROPERTY(QColor bgColor READ getBackgroundColor WRITE setBackgroundColor)
    Q_PROPERTY(QColor txtColor READ getTextColor WRITE setTextColor)

public:
    explicit QAbstractListWidget(QWidget *parent = nullptr);
    ~QAbstractListWidget();

    void clear();
    void addItem(QAbstractListItem *_item);
    void deleteItem(QAbstractListItem *_item);
    void setPrevIndex();
    void setNextIndex();
    void setCurrentIndex(int index);

    QColor getBackgroundColor() const;
    void setBackgroundColor(const QColor &color);

    QColor getTextColor() const;
    void setTextColor(const QColor &color);

signals:
    void itemClicked(QAbstractListItem *_item);

protected:
    QColor m_backgroundColor;
    QColor m_textColor;
    QString m_strEmptyHint;

    int    m_itemSize;
    int    m_itemSpace;
    QMap<int, QAbstractListItem*>   m_items;
    int    m_currentIndex;
    int    m_hoverIndex;
protected:
    void resizeEvent(QResizeEvent *e) override;
    void mouseMoveEvent(QMouseEvent *) override;
    void mousePressEvent(QMouseEvent *) override;

    void leaveEvent(QEvent *e) override;

    void paintEvent(QPaintEvent *) override;
    void drawIconfont(QPainter *painter, const QRect &_rect, QChar _ico, QColor _color, int _size = 20);
    virtual void drawItemInfo(QPainter *painter, QAbstractListItem *_item);

    Q_SLOT void adjustSize();
    virtual void clearRects();
};

//////////////////////////////////////////////////////////////////////////////////
class QAbstractListItem : public QObject {
    Q_OBJECT
public:
    QAbstractListItem();
    QAbstractListItem(int _id);

    int id() const;

    void setRect(const QRect &_rect);
    QRect rect();

    bool isHover() const;
    void setHover(bool bOk);

    bool isChecked();
    void setChecked(bool bOk);
protected:
    int m_id;
    QRect m_rect;

    bool m_hover;
    bool m_checked;
};

#endif // QABSTRACTLISTWIDGET_H
