﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qmp3info.h --- QMp3Info
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/16
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/16
 *******************************************************************/
#ifndef QMP3INFO_H
#define QMP3INFO_H

#include <QObject>
#include "qmusiclist.h"


/* mp3IDV2格式：-》标签头+标签帧1头帧+标签帧1内容1+标签帧2头帧+标签帧2内容+....+ 最后音乐正式内容 */
typedef struct TAB_info_st      // 标签头：开始前10位
{
    char format[3];             // 格式
    char version;               // 版本
    char unuse[2];
    char header_size[4];        // 标签帧+标签头的size
} TAB_info;

typedef struct head_info_st     // 标签帧头帧：每帧前8位
{
    char FrameID[4];    /* 用四个字符标识一个帧，说明其内容，稍后有常用的标识对照表 */
    char Size[4];       /* 帧内容的大小，不包括帧头，不得小于1 */
    char Flags[2];      /* 存放标志，只定义了6位，稍后详细解说 */
} HEAD_info;

class QMp3Info : public QObject
{
    Q_OBJECT
public:
    explicit QMp3Info(QObject *parent = nullptr);
    static bool analyseMusic(const QString &_path, QMusicItem *_item);
};

#endif // QMP3INFO_H
