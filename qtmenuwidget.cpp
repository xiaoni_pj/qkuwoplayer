﻿/******************************************************************
 *Company: http://www.xiaomutech.com/
 *fileName : qtmenuwidget.cpp --- QtMenuWidget
 *Auth       : yhni (QQ:393320854)
 *Create    : 2022/9/21
 *Description   :
 *Histroy:
 *<Auth>    <Date>        <Ver>        <Content>
 *         2022/9/21
 *******************************************************************/
#include "qtmenuwidget.h"

#include <QPainter>
#include <QBoxLayout>

QtMenuWidget::QtMenuWidget(QWidget *_centerWidget, QWidget *parent) : QMenu(parent)
{
    m_direction = Top;
    m_centerWidget = _centerWidget;
    setWindowFlags(this->windowFlags() | Qt::NoDropShadowWindowHint);

    if (NULL != _centerWidget)
    {
        QHBoxLayout *horlayout = new QHBoxLayout(this);
        horlayout->setContentsMargins(5, 5, 5, 5);
        horlayout->addWidget(_centerWidget);
    }
}

QtMenuWidget::~QtMenuWidget()
{
}

void QtMenuWidget::setDireciton(int _dir)
{
    m_direction = _dir;
    this->update();
}

void QtMenuWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.fillRect(this->rect(), QColor("#fafafa"));

    QPainterPath path;
    if (Left == m_direction)
    {
        path.addRoundedRect(8, 1, this->width() - 10, this->height() - 2, 5, 5);
        path.moveTo(8, this->height() * 0.5 - 4);
        path.lineTo(1, this->height() * 0.5);
        path.lineTo(8, this->height() * 0.5 + 4);
    }
    else if (Top == m_direction)
    {
        path.addRoundedRect(1, 8, this->width() - 2, this->height() - 10, 5, 5);
        path.moveTo(this->width() * 0.5 - 4, 8);
        path.lineTo(this->width() * 0.5, 1);
        path.lineTo(this->width() * 0.5 + 4, 8);
    }
    else if (Right == m_direction){
        path.addRoundedRect(1, 1, this->width() - 10, this->height() - 2, 5, 5);
        path.moveTo(this->width() - 8, this->height() * 0.5 - 4);
        path.lineTo(this->width() - 1, this->height() * 0.5);
        path.lineTo(this->width() - 8, this->height() * 0.5 + 4);
    }
    else if (Bottom == m_direction){
        QRect rect(1, 1, this->width() - 2, this->height() - 2 - 8);
        path.addRoundedRect(rect, 5, 5);
        path.moveTo(this->width() * 0.5 - 5, rect.bottom() + 1);
        path.lineTo(this->width() * 0.5, this->height() - 2);
        path.lineTo(this->width() * 0.5 + 5, rect.bottom() + 1);
    }

    path.closeSubpath();
    painter.setPen(QColor("#e1e1e1"));
    painter.setBrush(QColor("#ffffff"));
    painter.drawPath(path.simplified());
}

