#ifndef QICONFONTBUTTON_H
#define QICONFONTBUTTON_H

#include <QPushButton>

class QIconFontButton : public QPushButton
{
    Q_OBJECT
public:
    explicit QIconFontButton(QWidget *parent = nullptr);

    Q_PROPERTY(QString iconfont READ iconfont WRITE setIconfont)
signals:

public:
    QString iconfont() const;
    void setIconfont(const QString &_txt);

private:
    QString m_iconfont;
};

#endif // QICONFONTBUTTON_H
